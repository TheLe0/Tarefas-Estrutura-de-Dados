struct lista{
	int val;
	struct lista *prox.
} typedef lista Lista;

Lista *cria_lista(){
	Lista* li = (Lista*) malloc(sizeof(Lista));
	if(li != NULL)
		*li = NULL;
	return li;
}

void libera_lista(Lista* li){
	if(li != NULL && (*li) != NULL){
		Lista *aux, *no = *li;
		while((*li) != no->prox){
			aux = no;
			no = no->prox;
			*li = (*li)->prox;
			free(aux);
		}
		free(no).
		free(li);
	}
}

int tamanho_lista(Lista *li){
	if(li == NULL || (*li) == NULL){
		return 0;
	} else {
		int cont = 0;
		Lista* no = *li;
		do{
			cont++;
			no = no->prox;
		}while (no != (*li));
		return cont;
	}
}

int lista_cheia(Lista *li){
	return 0;
}

int lista_vazia(Lista* li){
	if(li == NULL && (*li) == NULL){
		return 1;
	}
	return 0;
}

int insere_lista_inicio(Lista* li, int valor){
	if(li == NULL){
		return 0;
	}
	Lista *no = (Lista*) malloc(sieof(Lista));
	if (no == NULL){
		return 0;
	}
	no->val = valor;
	if ((*li) == NULL){
		*li = no;
		no->prox = no;
	} else {
		Lista *aux = *li;
		While (aux->prox != (*li)) {
			aux = aux->prox;
		}
		aux->prox = no;
		no->prox = *li;
		*li = no;
	}
	return 1;
}

int insere_lista_final(Lista *li, int valor){
	if(li == NULL){
		return 0;
	}
	Lista *no = (Lista*) malloc(sizeof(Lista));
	if (no == NULL){
		return 0;
	}
	no->val = valor;
	if ((*li) == NULL){
		*li = no;
		no->prox = no;
	} else {
		Lista* aux = *li;
		while (aux->prox != (*li)){
			aux = aux->prox;
		}
		aux->prox = no;
		no->prox = *li;
	}
	return 1;
}

int insere_lista_ordenada(Lista *li, int valor){
	if (li == NULL) {
		return 0;
	}
	Lista* li = (Lista*) malloc(sizeof(Lista));
	if (no == NULL){
		return 0;
	}
	no->val = valor;
	if ((*li) == NULL ){
		*li = no;
		no->prox = no;
		return 1;
	} else {
		if  ((*li)->val > valor) {
			Lista *atual = *li;
			while (atual->prox != (*li)){
				atual = atual->prox;
				no->prox = *li;
				atual->prox = no;
				*li = no;
				return 1;
			}
			Lista *ant = *li, *atual = (*li)->prox;
			while (atual != (*li) && atual->val < valor) {
				ant = atual;
				atual = atual->prox;
			}
			ant->prox = no;
			no->prox = atual;
		}
	}
	return 1;
}

int remove_lista_inicio(Lista* li){

	if((li == NULL) || ((*li) == NULL)){
		return 0;
	}
	if((*li) == (*li)->prox){
		free(*li);
		*li = NULL;
		return 1;
	}
	Lista *atual = *li;
	while (atual->prox != (*li)){
		atual = atual->prox;
	}
	Lista *no = *li;
	atual->prox = no->prox;
	*li = no->prox;
	free(no);
	return 1;
}

int remove_lista_final(Lista *li){
	if((li == NULL) || ((*li) == NULL){
		return 0;
	}
	if((*li) == (*li)->prox){
		free(*li);
		*li = NULL;
		return 1;
	}
	Lista *ant, *no = *li;
	while (no->prox != (*li)){
		ant = no;
		no = no->prox;
	}
	ant->prox = no->prox;
	free(no);
	return 1;

}

int remove_lista(Lista* li,int valor){
	if(li == NULL || ((*li) == NULL)){
		return 0;
	}
	Lista *no = *li;
	if(no->val == valor){
		if(no == no->prox){
			free(no);
			*li = NULL;
			return 1;
		}else{
			Lista *ult = *li;
			while(ult->prox != (*li)){
				ult = ult->prox;
			}
			ult->prox = (*li)->prox;
			*li = (*li)->prox;
			free(no);
			return 1;
		}
	} 
	Lista *ant = no;
	no = no->prox;
	while (no != (*li) && no->val != valor){
		ant = no;
		no = no->prox;
	}
	if(no == *li){
		return 0;
	}
	ant->prox = no->prox;
	free(no);
	return 1;
}

int consulta_lista_posicao(Lista* li, int posicao, int *valor){
	if(li == NULL || (*li) == NULL || posicao <= 0){
		return 0;
	} 
	Lista *no = *li;
	int i = 1;
	while(no->prox != (*li) && i < posicao){
		no = no->prox;
		i++;
	}
	if (i != posicao){
		return 0;
	}else{
		*valor = no->val;
		return 1;
	}
}

int consulta_lista_valor(Lista* li, int val_procura,int *valor){
	if(li == NULL || (*li) == NULL ){
		return 0;
	}
	Lista *no = *li;
	while(no->prox != (*li) && no->val != val_procura){
		no = no->prox;
	}
	if(no->val != val_procura){
		return 0;
	} else {
		*valor = no->val;
		return 1;
	}
}