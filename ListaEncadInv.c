struct lista{
	int val;
	struct lista *prox;
};
typedef struct lista Lista;

Lista *cria_lista(){
	Lista* li = (Lista*) malloc(sizeof(Lista));
	if(li != NULL)
		*li = NULL;
	return li;
}

void libera_lista(Lista* li){
	if(li != NULL){
		Lista *no;
		while((*li) != NULL){
			no = *li;
			*li = (*li)->prox;
			free(no);
		}
		free(li);
	}
}

Lista  *busca (int x, Lista *ini)
{
   Lista *p;
   p = ini->prox;
   while (p != NULL && p->val != x) 
      p = p->prox; 
   return p; 
}

int insere_inicio_unico(Lista* li){
	if(li == NULL || *li == NULL){
		Lista *no = (Lista*) malloc(sizeof(Lista));
		no->val = valor;
		no->prox = (*li);
		*li = no;
		return 1;	
	}
	return 0;
}

int insere_lista_inicio(Lista* li,int valor){
	if(li == NULL) return 0;
	Lista* no = (Lista*) malloc(sizeof(Lista));
	if(no == NULL) return 0;
	no->val = valor;
	no->prox = (*li);
	*li = no;
	return 1;	
}

int insere_lista_final(Lista *li, int valor){
	if(li == NULL) return 0;
	Lista *no = (Lista*) malloc(sizeof(Lista));
	if(no == NULL) return 0;
	no->val = valor;
	no->prox = NULL;
	if((*li) == NULL ){
		*li = no;
	}else{
		Elem *aux = *li;
		while(aux->prox != NULL){
			aux = aux->prox;
		}
		aux->prox = no;
	}
	return 1;
}

int insere_lista_ordenada(Lista *li,int valor){
	if(li == NULL) return 0;
	Lista *no = (Lista*) malloc(sizeof(Lista));
	if(no == NULL) return 0;
	no->val = valor;
	if(lista_vazia(li)){
		no->prox = (*li);
		*li = no;
		return 1;
	}else{
		Lista *ant, *atual = *li;
		while(atual != NULL && atual->val < no.val){
			ant = atual;
			atual = atual->prox;
		}
		if(atual == *li){
			no->prox = (*li);
			*li = no;
		}else{
			no->prox = ant->prox;
			ant->prox = no;
		}
		return 1;
	}
}

void inverte (Lista* li) {

	Lista* p;
	Lista* q;

	q= cria_lista();

	for (p = l; p != NULL; p = p->prox){

		q=insere_lista_final(q,p->val);
		q=q->prox;

	}

}

int remove_lista_inicio(Lista *li){
	if(li == NULL){
		return 0;
	}
	if((*li) == NULL){
		return 0;
	}
	
	Lista *no = *li;
	*li = no->prox;
	free(no);
	return 1;
}
int remove_lista_final(Lista *li){
	if(li == NULL) return 0;
	if((*li) == NULL)return 0;
	Lista *ant, *no = *li;
	while(no->prox != NULL){
		ant = no;
		no = no->prox;
	}
	if(no == (*li)){
		*li = no->prox;
	}else
		ant->prox = no->prox;
	free(no);
	return 1;
}
int remove_lista(Li *li,int valor){
	if(li == NULL) return 0;
	Lista *ant, *no = *li;
	while(no != NULL && no->val != valor){
		ant = no;
		no = no->prox;
	}
	if(no == NULL) return 0;
	if(no == *li)
		*li = no->prox;
	else
		ant->prox = no->prox;
	free(no);
	return 1;
}