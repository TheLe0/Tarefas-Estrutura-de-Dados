#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<string.h>

#define NOME 50
#define TAM 10

struct funcionario{
   char nome[NOME];
   int idade;
};
struct funcionario lfunc[TAM]; //defini��o do vetor de funcion�rios
int qtde=0; //controla a quantidade de elementos do vetor

void insere(struct funcionario dados_func){ // Insere no vetor os dados do funcion�rio.
     if(qtde < TAM){
        lfunc[qtde] = dados_func;
        qtde++;
     }
}

void listar(){ //Lista os funcion�rios
    for (int i=0; i<qtde; i++){
      printf("-----------------------------------\n");
      printf("Nome do %uo Funcionario: %s ", i+1, lfunc[i].nome);
      printf("\nIdade do %uo Funcionario: %u \n", i+1, lfunc[i].idade);
      printf("-----------------------------------\n");
      printf("\n");
   } 
}
int pesquisa(struct funcionario dados_func){ 
//Pesquisa um determinado funcion�rio. Se ele existir, retorna a sua 
// posi��o no vetor. Se n�o existir retorna -1
    int achou = 0;
    int i = 0;
    int pos = -1;
    while((achou==0) && (i<qtde)){
      if(strcmp(dados_func.nome, lfunc[i].nome) == 0){
         achou = 1;
         pos = i;
      }
      else{
         i++;
      }
   }
   return pos;
}

void exclui(struct funcionario dados_func){ // Exclui um funcionario
   int i, j;
   int pos = pesquisa(dados_func);
   
   if(pos >=0){ //achou o funcionario na lista
      for(int j=pos;j<qtde-1;j++){
         lfunc[j] = lfunc[j+1];
      }
      qtde--;
   }
}

void opcoes(){
     printf("\n");
     printf("1 - Inserir Funcionario\n");
     printf("2 - Listar Funcionarios\n");
     printf("3 - Excluir Funcionario\n");
     printf("0 - Sair\n");
}

void menu(){
     struct funcionario dados_func;
     char opc;
     system("cls");
     opcoes();
     opc = getch();
     while(opc != '0'){
          switch(opc){
             case '1':  printf("\nNome do Funcionario: ");
                        fflush(stdin);
                        scanf("%s", &dados_func.nome);

                        printf("Idade do Funcionario: ");
                        fflush(stdin);
                        scanf("%u", &dados_func.idade);
                        insere(dados_func);
                        break;
                        
             case '2':  listar();
                        break;
                        
             case '3':  printf("\nNome do Funcionario: ");
                        fflush(stdin);
                        scanf("%s", &dados_func.nome);
                        exclui(dados_func);
                        break;  
          }
          opcoes();
          opc = getch();     
     }
}

int main(){
   menu();
   system("pause");
   return 0;
}
