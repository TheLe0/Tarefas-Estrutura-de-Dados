#include <iostream>
#include<stdlib.h>
using namespace std;
////////////////////////////////////////////////////////////////
class Nodo
   {
   public:
      Nodo* ant; 
      int dados;                  
      Nodo* prox;                   
//-------------------------------------------------------------
   Nodo(int pdado) : ant(NULL),dados(pdado), prox(NULL)  
      {  }
   void setAnt(Nodo* pant){
        ant = pant;
   }
   Nodo* getAnt(){
         return ant;
   }   
   void setDado(int pdado){
        dados = pdado;
   }   
   int getDado(){
       return dados;
   }
   void setProx(Nodo* pprox){
        prox = pprox;
   }
   Nodo* getProx(){
         return prox;
   }   
//-------------------------------------------------------------
   void displayLink()             
      { cout << dados << " "; }
   };  //end class Nodo
////////////////////////////////////////////////////////////////
class Lista
   {
   private:
      Nodo* inicio; 
      Nodo* fim;                
   public:
//-------------------------------------------------------------
   Lista() : inicio(NULL), fim(NULL)      
      {  }
//-------------------------------------------------------------
   ~Lista(){                                            
      Nodo* atual = inicio;       
      while(atual != NULL){       
         Nodo* aux = atual; 
         atual = atual->prox;
         delete aux;           
      }
   }
//-------------------------------------------------------------
   bool isEmpty(){                    
       return (inicio==NULL);
   }
//-------------------------------------------------------------
   void insereInicio(int key){
      Nodo* novo = new Nodo(key); 
      if(isEmpty()){          
         inicio = novo;
         fim = novo;
      }
      else{
         inicio->setAnt( novo );
         novo->setProx( inicio );  
         inicio = novo;
      }
   }   
//-------------------------------------------------------------
   void insereFim(int key){
      Nodo* novo = new Nodo(key); 
      if(isEmpty()){
         inicio = novo;
         fim = novo;
      }
      else{
          fim->setProx( novo );
          novo->setAnt( fim );
          fim = novo;
      } 
   }   
//-------------------------------------------------------------
   void insereOrdenado(int key){   
      Nodo* novo = new Nodo(key);
      Nodo* anterior = NULL;  
      Nodo* atual = inicio;
      
      if(key > fim->getDado())
             insereFim(key);
      else{
         if( key < inicio->getDado())
               insereInicio(key);
         else{                
             while(atual != NULL && key > atual->getDado())
             {    
                anterior = atual;
                atual = atual->getProx(); 
             }
             if(anterior==NULL)       //um elemento
                inicio = novo;
             else{
                novo->setProx(atual);
                novo->setAnt(anterior);
                anterior->setProx(novo);
                atual->setAnt(novo);
             }
         }
      }    
   }  //end insert()
//-------------------------------------------------------------
   void removeInicio(){
      if(isEmpty()){
        cout << "Lista Vazia";
        return;
      }
      else{ 
            if( inicio == fim ){
                inicio = NULL;
                fim = NULL;
            }
            else{
               Nodo* aux = inicio;
               inicio = inicio->getProx();
               inicio->setAnt(NULL);
               delete aux;
            }            
      }
   }
//-------------------------------------------------------------
   void removeFim(){
      if(isEmpty()){
        cout << "Lista Vazia";
        return;
      }
      else{
           if( inicio == fim ){
                inicio = NULL;
                fim = NULL;
            }
            else{
                 Nodo* atual = fim;
                 Nodo* aux = fim;
                 
                 atual = atual->getAnt();
                 atual->setProx(NULL);
                 fim = atual;
           
                 delete aux;
           }
      }
   }
//-------------------------------------------------------------

  void removePos( int ind ){
      int i=0;
      
      Nodo* aux = inicio;
      while( (aux!=NULL) && (i!= ind)){
             aux = aux->getProx();
             i++;
      }
      if(aux==NULL){
         cout << "Posicao nao encontrada" << endl;
      }
      else{
           if( aux->getAnt()==NULL){ //primeiro elemento
               removeInicio();
           }
           else{
                if(aux->getProx()==NULL){  //ultimo elemento
                   removeFim();
                }
                else{ // meio da lista
                   Nodo* ant = aux->getAnt();
                   Nodo* post = aux->getProx();
                   
                   ant->setProx(aux->getProx());
                   post->setAnt(ant->getAnt());
                   delete aux;
                }
           }
      }
       
  }

//-------------------------------------------------------------
   void displayList(){
      cout << "Lista (primeiro-->ultimo): ";
      Nodo* atual = inicio; 
      while(atual != NULL){
         atual->displayLink(); 
         atual = atual->getProx(); 
      }
      cout << endl;
   } 
//-------------------------------------------------------------
   void displayList2(){
      cout << "Lista (ultimo-->primeiro): ";
      Nodo* atual = fim; 
      while(atual != NULL){
         atual->displayLink(); 
         atual = atual->getAnt(); 
         }
      cout << endl;
      }
   }; 
////////////////////////////////////////////////////////////////
int main()
   {                            
   Lista lista;    //create new list
   lista.insereFim(10);
   lista.insereFim(20);
   lista.insereFim(30);
   lista.insereFim(40);
  
   lista.insereOrdenado(15);
   lista.insereOrdenado(45);
   lista.displayList(); 

   lista.removeInicio();
   lista.displayList();
   
   lista.removeFim();
   lista.displayList();
   lista.displayList2();
   
   system("pause");
   return 0;
   }  //end main()
