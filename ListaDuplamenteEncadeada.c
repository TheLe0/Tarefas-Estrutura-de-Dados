#include <stdio.h>
#include <stdlib.h>


struct valor {
	int val;
};
typedef struct elemento* Lista;

struct elemento {
	struct valor dados;
	struct elemento *ant;
	struct elemento *prox;
};
 typedef struct elemento Elem;


 Lista* cria_lista(){
 	Lista* li = (Lista*) malloc(sizeof(Lista));
 	if (li != NULL) {
 		*li = NULL;
 	}
 	return li;
 }

 void libera_lista(Lista* li) {

 	if(li != NULL) {
 		Elem* no;
 		while ((*li) != NULL) {
 			no = *li;
 			*li = (*li)->prox;
 			free(no);
 		}
 		free(li);
 	}

 }

 int tamanho_lista(Lista* li){
 	if(li == NULL){
 		return 0;
 	}
 	int cont = 0;
 	Elem* no = *li;
 	while(no != NULL) {
 		cont++;
 		no = no->prox;
 	}
 	return cont;
 }

 int lista_cheia(Lista* li){
 	return 0;
 }

 int insere_inicio(Lista* li,struct valor val){
 		if(li == NULL){
 			return 0;
 		}
 		Elem* no = (Elem*) malloc(sizeof(Elem));
 		if (no == NULL) {
 			return 0;
 		}
 		no->dados = val;
 		no->prox = (*li);
 		no->ant = NULL;

 		if(*li != NULL){
 			(*li)->ant = no;
 		}
 		*li = no;
 		return 1;
 }


 int insere_final(Lista* li,struct valor val){
 	if(li == NULL){
 		return 0;
 	}
 	Elem *no = (Elem*) malloc(sizeof(Elem));
 	if (no == NULL) {
 		return 0;
 	}
 	no->dados = val;
 	no->prox = NULL;
 	if((*li) == NULL){
 		no->ant = NULL;
 		*li = no;
 	}else{
 		Elem *aux = *li;
 		while(aux->prox != NULL){
 			aux = aux->prox;
 		}
 		aux->prox = no;
 		no->ant = aux;
 	}
 	return 1;
 }

 int insere_ordenada(Lista* li,struct valor valr){
 	if(li == NULL){
 		return 0;
 	}
 	Elem *no = (Elem*) malloc(sizeof(Elem));
 	if(no == NULL){
 		return 0;
 	}
 	no->dados = valr;
 	if(tamanho_lista(li) == 0){
 		no->prox = NULL;
 		no->ant = NULL;
 		*li = no;
 		return 1;
 	} else {
 		Elem *ante, *atual = *li;
 		while (atual != NULL && atual->dados.val < valr.val) {
 			ante = atual;
 			atual = atual->prox;
 		}
 		if(atual == *li){
 			no->ant = NULL;
 			(*li)->ant = no;
 			no->prox = (*li);
 			*li = no;
 		} else {
 			no->prox = ante->prox;
 			no->ant = ante;
 			ante->prox = no;
 			if(atual != NULL){
 				atual->ant = no;
 			}
 			return 1;
 		}
 	}
 }

 int remove_inicio(Lista* li){
 	if(li == NULL){
 		return 0;
 	}
 	if((*li) == NULL){
 		return 0;
 	}
 	Elem *no = *li;
 	*li = no->prox;

 	if(no->prox != NULL){
 		no->prox->ant = NULL;
 	}
 	free(no);
 	return 1;
 }

 int remove_final(Lista* li){
 	if(li == NULL){
 		return 0;
 	}
 	if((*li) == NULL){
 		return 0;
 	}
 	Elem* no = *li;
 	while(no->prox != NULL){
 		no = no->prox;
 	}

 	if(no->ant == NULL){
 		*li = no->prox;
 	} else {
 		no->ant->prox = NULL;
 	}

 	free(no);
 	return 1;
 }

 int remove_ordenada(Lista* li, int valr){
 	if(li == NULL){
 		return 0;
 	}
 	Elem *no = *li;
 	while(no != NULL && no->dados.val != valr){
 		no = no->prox;
 	}
 	if(no == NULL){
 		return 0;
 	}
 	if(no->ant == NULL){
 		*li = no->prox;
 	} else {
 		no->ant->prox = no->prox;
 	}

 	if(no->prox != NULL){
 		no->prox->ant = no->ant;
 	}
 	free(no);
 	return 1;
 }

 int main() {

 	int opt,rep,opcao,valor,valr;
 	Elem l = cria_lista();
 	Lista value;

 	rep = 1;

 	while(rep == 1){

 		printf
 		("
 			1- Adicionar elemento na lista;
 			2- Remover elemento na lista;
 			3- Sair.

 		");
 		scanf("%d",&opt);

 		switch(opt){

 			case: 1
 				printf("Digite o valor: ");
 				scanf("%d",&value.val);

 				if(tamanho_lista(l) == 0){
 					insere_inicio(l,value);
 				} else {
 					insere_ordenada(l,value);
 				}
 				break;
 			case: 2
 				if(tamanho_lista(l) == 0){
 					printf("Não pode ser removido elementos de uma lista vazia!");
 					return 0;
 				} else {
 					printf("
 							1 - Remover no Início;
 							2 - Remover no Final;
 							3 - Remover Elemento Específico.
 						");
 					scanf("%d",&opcao);

 					switch(opcao){
 						case: 1
 							remove_inicio(l);
 							break;
 						case: 2
 							remove_final(l);
 							break;
 						case: 3
 							printf("Digite o valor a ser removido: ");
 							scanf("%d",valr);
 							if(remove_ordenada(l,valr) != 1){
 								printf("Valor não encontrado.");
 							}
 							break;
 						default:
 							printf("Opção Inválida!");
 							return 0;
 					}
 				}
 				break
 			case: 3
 				rep = 0;
 				break;
 			default:
 				printf("Opção Inválida!");
 		}
 	}
 	libera_lista(l);
 	system("pause");
 }