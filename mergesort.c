#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAM 50000000
char *vout;

void merge(char vin[],int ini1, int fim1, int ini2, int fim2)
{
     int i,isai;
     int inicio=ini1;
     for (isai=ini1;isai<=fim2;isai++)
        if (ini1>fim1)
           vout[isai]=vin[ini2++];
        else if (ini2>fim2)
                vout[isai]=vin[ini1++];
             else if (vin[ini1]<vin[ini2])
                     vout[isai]=vin[ini1++];
                  else vout[isai]=vin[ini2++];
     for (i=inicio;i<=fim2;i++) vin[i]=vout[i];
        
}
                  
void mergesort(char v[],int ini, int fim)
{
     int i;
     if (ini<fim) {
        int meio=(fim+ini)/2;
        mergesort(v,ini,meio);
        mergesort(v,meio+1,fim);
        merge(v,ini,meio,meio+1,fim);
    }  
               
 }

int main() {

	int i;
    char *v,*w;
    time_t t1,t2;
    srand(time(NULL));
    v=(char *)malloc(TAM);
    w=(char *)malloc(TAM);
    vout=(char *)malloc(TAM);
    time(&t1);
    for (i=0;i<TAM;i++) {
	   v[i]=w[i]=rand()%100;
    }
    time(&t2);
    printf("\n Gerou os dados em %d segundos \n",t2-t1);
    time(&t1);
    mergesort(v,0,TAM-1);
    time(&t2);
    printf("\n Ordenado por merge em %d segundos \n",t2-t1);

    system("pause");
}
