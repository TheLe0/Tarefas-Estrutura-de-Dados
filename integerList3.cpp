#include <iostream>
#include<stdlib.h>
using namespace std;
////////////////////////////////////////////////////////////////
class Nodo
   {
   public:
      int dados;                  
      Nodo* prox;                   
//-------------------------------------------------------------
   Nodo(int pdado){
            dados = pdado;
            prox = NULL;
   }
   void setDado(int pdado){
        dados = pdado;
   }   
   int getDado(){
       return dados;
   }
   void setProx(Nodo* pprox){
        prox = pprox;
   }
   Nodo* getProx(){
         return prox;
   }   
//-------------------------------------------------------------
   void displayLink()             
      { cout << dados << " "; }
   };  //end class Nodo
////////////////////////////////////////////////////////////////
class Lista
   {
   private:
      Nodo* inicio;                 
   public:
//-------------------------------------------------------------
   Lista(){ 
      inicio = NULL;
   }
//-------------------------------------------------------------
   ~Lista()                     
      {                          
      Nodo* atual = inicio;       
      while(atual != NULL)       
         {
         Nodo* aux = atual; 
         atual = atual->prox;
         delete aux;           
         }
      }
//-------------------------------------------------------------
   bool isEmpty()                    
      { return (inicio==NULL); }
//-------------------------------------------------------------
   void insereInicio(int key){
      Nodo* novo = new Nodo(key); 
      if(isEmpty()){
         inicio = novo;
      }
      else{
         novo->setProx(inicio);
         inicio = novo;
      }
   }   

//-------------------------------------------------------------
   void insereFim(int key){
      Nodo* novo = new Nodo(key); 
      Nodo* atual = inicio;
      if(isEmpty()){
         inicio = novo;
      }
      else{
          while( atual->getProx() != NULL){
                 atual = atual->getProx();
          }
          atual->setProx(novo); 
      } 
   }   
     
//-------------------------------------------------------------
   void insereOrdenado(int key){   
      Nodo* novo = new Nodo(key);
      Nodo* anterior = NULL;  
      Nodo* atual = inicio;
                                     
      while(atual != NULL && key > atual->getDado())
         {                           
         anterior = atual;
         atual = atual->getProx(); 
         }
      if(anterior==NULL)       
         inicio = novo;          
      else                         
         anterior->setProx(novo); 
      novo->setProx(atual);    
      }  //end insert()
//-------------------------------------------------------------
   void removeInicio(){
      if(isEmpty()){
        cout << "Lista Vazia";
        return;
      }
      else{                                       
          Nodo* aux = inicio;
          inicio = inicio->getProx(); 
          delete aux; 
      }
   }
//-------------------------------------------------------------
   void removeFim(){
      if(isEmpty()){
        cout << "Lista Vazia";
        return;
      }
      else{
          Nodo* anterior = NULL; 
          Nodo* atual = inicio; 
          
          while( atual->getProx() != NULL){
                 anterior = atual;
                 atual = atual->getProx();
          } 
          anterior->setProx(NULL);
          delete atual; 
      }
   }

//-------------------------------------------------------------
   void displayList(){
      cout << "Lista (primeiro-->ultimo): ";
      Nodo* atual = inicio; 
      while(atual != NULL)
         {
         atual->displayLink(); 
         atual = atual->getProx(); 
         }
      cout << endl;
      }
   }; 
////////////////////////////////////////////////////////////////
class Testa
   {
   private:
      Lista lista;                 
   public:
   Testa(){
      
      lista.insereInicio(40);    //insert 2 items
      lista.insereInicio(20);
      lista.insereFim(60);
      lista.insereOrdenado(50);

      lista.displayList(); 

      lista.removeInicio();
      lista.displayList();
   
      lista.removeFim();
      lista.displayList();
   }
 };
  
 int main(){                            
       Testa teste;    //create new list
       system("pause");
       return 0;
 }  //end main()


