#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct valor{
	char va;
};
typedef struct valor* Pilha;

struct elemento{
	struct valor dados;
	struct elemento* prox;
};
typedef struct elemento Elem;


Pilha* cria_pilha(){
	Pilha* pi = (Pilha*) malloc(sizeof(Pilha));
	if( pi == NULL){
		*pi = NULL;
	}
	return pi;
}

void libera_pilha(Pilha* pi){
	if(pi != NULL){
		Elem* no;
		while((*pi) != NULL ) {
			no = *pi;
			*pi = (*pi)->prox;
			free(no);
		}
		free(pi);
	}
}

int insere_pilha(Pilha *pi, struct valor val){
	if(pi == NULL){
		return 0;
	}
	Elem* no = (Elem*) malloc(sizeof(Elem));
	if (no == NULL){
		return 0;
	}
	no->va = val;
	no->prox = (*pi);
	return 1;

}

int remove_pilha(Pilha* pi){
	if(pi == NULL || (*pi) == NULL){
		return 0;
	}
	Elem *no = *pi;
	*pi = no->prox;
	free(no);
	return 1;
}  

int valor_topo(Pilha* pi){
	if(pi == NULL){
		return 0;
	}
	Pilha p;
	p = cria_pilha();
	while(pi->prox != NULL){
		p = pi->prox;
	}
	return char_to_int(pi->va);
}

int valor_topo_operador(Pilha* pi){
	if(pi == NULL){
		return 0;
	}
	Pilha p;
	p = cria_pilha();
	while(pi->prox != NULL){
		p = pi->prox;
	}
	return pi->va;
}

int verifica_exp(char exp[10]){
	int i;
	for(i = 0; i<=strlen(exp);i++){
		if(
			exp[i] != "0" &&
			exp[i] != "1" &&
			exp[i] != "2" &&
			exp[i] != "3" &&
			exp[i] != "4" &&
			exp[i] != "4" &&
			exp[i] != "4" &&
			exp[i] != "5" &&
			exp[i] != "6" &&
			exp[i] != "7" &&
			exp[i] != "8" &&
			exp[i] != "9" &&
			exp[i] != "+" &&
			exp[i] != "-" &&
			exp[i] != "*" &&
			exp[i] != "(" &&
			exp[i] != ")"
		){
			return 0;
		} else {
			return 1;
		}

	}

}

int opera(int op1, int op2, char opc){

	switch(opc){

		case "+":
			return op1 + op2;
			break;
		case "-":
			return op1 - op2;
			break;
		case "*":
			return op1*op2;
			break;
	}
}

int char_to_int(char valor){
	switch(valor){

		case "0":
			return 0;
			break;
		case "1":
			return 1;
			break;
		case "2":
			return 2;
			break;
		case "3":
			return 3;
			break;
		case "4":
			return 4;
			break;
		case "5":
			return 5;
			break;
		case "6":
			return 6;
			break;
		case "7":
			return 7;
			break;
		case "8":
			return 8;
			break;
		case "9":
			return 9;
			break;
	}
}

int is_num(char exp){
	if(
		exp != "+" &&
		exp != "-" &&
		exp != "*" &&
		exp != "(" &&
		exp != ")"
	){
		return 0;
	}else{
		return 1;
	}
}

int main(){

	Pilha *operadores;
	Pilha *operandos;
	operandos = cria_pilha();
	operadores = cria_pilha();
	char exp[10];
	int op1, op2;
	char op3;
	int result;

	printf("Digite uma exprecao matematica: ");
	gets(exp);

	if(verifica(exp)){
		int i = 0;
		while(i <= strlen(exp)){

			if(is_num(exp[i])){
				insere_pilha(operadores,exp[i]);
			} else {
				insere_pilh(operandos,exp[i]);
			}

			if(exp[i] == ")"){
				op1 = valor_topo(operandos);
				remove_pilha(operandos);
				op2 = valor_topo(operandos);
				remove_pilha(operandos);
				op3 = valor_topo_operador(operadores);
				remove_pilha(operadores);
				result = opera(op1,op2,op3);
				insere_pilha(operandos,result);
			}
			i++;
		}
	}else {
		printf("Exprecao invalida!");
	}
	printf("O valor eh: %d",valor_topo(operandos));
	libera_pilha(operandos);
	libera_pilha(operadores);
	system("pause");
}