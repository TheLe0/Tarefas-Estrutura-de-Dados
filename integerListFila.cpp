#include <iostream>
#include<stdlib.h>
using namespace std;
////////////////////////////////////////////////////////////////
class Nodo
   {
   public:
      int dados;                  
      Nodo* prox;                   
//-------------------------------------------------------------
   Nodo(int pdado) : dados(pdado), prox(NULL)  
      {  }
   void setDado(int pdado){
        dados = pdado;
   }   
   int getDado(){
       return dados;
   }
   void setProx(Nodo* pprox){
        prox = pprox;
   }
   Nodo* getProx(){
         return prox;
   }   
//-------------------------------------------------------------
   void displayLink()             
      { cout << dados << " "; }
   };  //end class Nodo
////////////////////////////////////////////////////////////////
class Fila
   {
   private:
      Nodo* inicio;
      Nodo* fim;                 
   public:
//-------------------------------------------------------------
   Fila() : inicio(NULL),fim(NULL)      
      {  }
//-------------------------------------------------------------
   ~Fila()                     
      {                          
      Nodo* atual = inicio;       
      while(atual != NULL){
         Nodo* aux = atual; 
         atual = atual->prox;
         delete aux;           
         }
      }
//-------------------------------------------------------------
   bool isEmpty()                    
      { return (inicio==NULL); }
//-------------------------------------------------------------
   void insere(int key){
      Nodo* novo = new Nodo(key); 
      Nodo* atual = inicio;
      if(isEmpty()){
         inicio = novo;
         fim=novo;
      }
      else{
          fim->setProx(novo);
          fim=novo;
      } 
   }   
     
//-------------------------------------------------------------
   void remove(){
      if(isEmpty()){
        cout << "Fila Vazia";
        return;
      }
      else{  
          Nodo* aux = inicio;                                     
          if(inicio==fim){ //um so elemento
             inicio = NULL;
             fim = NULL;
          }
          else{
               inicio = inicio->getProx(); 
          }
          delete aux;
      }
   }

//-------------------------------------------------------------
   void displayList(){
      cout << "Lista (primeiro-->ultimo): ";
      Nodo* atual = inicio; 
      while(atual != NULL)
         {
         atual->displayLink(); 
         atual = atual->getProx(); 
         }
      cout << endl;
      }
   }; 
////////////////////////////////////////////////////////////////
int main(){                     
   Fila fila;    //create new list
   fila.insere(10);
   fila.insere(20);
   fila.insere(30);
   
   fila.displayList(); 

   fila.remove();
   fila.displayList();
   
   system("pause");
   return 0;
   }  //end main()
