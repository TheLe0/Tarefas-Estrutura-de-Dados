#include <iostream>
#include<stdlib.h>
using namespace std;
////////////////////////////////////////////////////////////////
class Nodo
   {
   public:
      int dados;                  
      Nodo* ant;                   
//-------------------------------------------------------------
   Nodo(int pdado) : dados(pdado), ant(NULL)  
      {  }
   void setDado(int pdado){
        dados = pdado;
   }   
   int getDado(){
       return dados;
   }
   void setAnt(Nodo* pant){
        ant = pant;
   }
   Nodo* getAnt(){
         return ant;
   }   
//-------------------------------------------------------------
   void displayLink()             
      { cout << dados << " "; }
   };  //end class Nodo
////////////////////////////////////////////////////////////////
class Pilha{
   private:
      Nodo* topo;                 
   public:
//-------------------------------------------------------------
   Pilha() : topo(NULL)      
      {  }
//-------------------------------------------------------------
   ~Pilha(){                          
      Nodo* atual = topo;       
      while(atual != NULL){
         Nodo* aux = atual; 
         atual = atual->ant;
         delete aux;           
      }
   }
//-------------------------------------------------------------
   bool isEmpty()                    
      { return (topo==NULL); }
//-------------------------------------------------------------
   void push(int key){
      Nodo* novo = new Nodo(key); 
      if(isEmpty()){
         topo = novo;
      }
      else{
         novo->setAnt(topo);
         topo = novo;
      }
   }   

//-------------------------------------------------------------
   int pop(){
       //retorna o valor e desempilha
      int res = 0;
      if(isEmpty()){
        cout << "Pilha Vazia";
      }
      else{                                       
          Nodo* aux = topo;
          res = topo->getDado();
          topo=topo->getAnt(); 
          delete aux; 
      }
      return res;
   }
//-------------------------------------------------------------
   int peek(){
       //retorna o valor 
      int res = 0;
      if(isEmpty()){
        cout << "Pilha Vazia";
      }
      else{                                       
          res = topo->getDado();
      }
      return res;
   }

//-------------------------------------------------------------
   void displayList(){
      cout << "Pilha (primeiro-->ultimo): ";
      Nodo* atual = topo; 
      while(atual != NULL){
         atual->displayLink(); 
         atual = atual->getAnt(); 
      }
      cout << endl;
   }
 }; 
////////////////////////////////////////////////////////////////
int main()
   {                            
   Pilha pilha;
   pilha.push(40);
   pilha.push(30);
   pilha.push(20);

   pilha.displayList(); 

   int x = pilha.pop();
   cout << "Retirado: " << x << endl;
   pilha.displayList();
   
   int y = pilha.peek();
   cout << "Consultado: " << y << endl;
   pilha.displayList();
   
   system("pause");
   return 0;
   }  //end main()
