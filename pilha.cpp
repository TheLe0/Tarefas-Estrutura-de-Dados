#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

struct nodo{
  int info;
  struct nodo *ant;
}elemento;

struct nodo *topo;

// Procedimento para inser��o no inicio da pilha
void push(int dado)
{
  struct nodo *pnovo;
  pnovo=(struct nodo*)malloc(sizeof(elemento));
  pnovo->info=dado;
  pnovo->ant=NULL;  
  if(topo == NULL){
     topo = pnovo;
  }
  else{
     pnovo->ant=topo;
	 topo=pnovo;   
  }
}


// Procedimento para excluir um valor da pilha
struct nodo * pop()
{
  int resp = 0;  
  struct nodo *atual = topo;
  if(atual != NULL){
     topo = topo->ant; //topo aponta para o pr�ximo elemento
     atual->ant = NULL;
  }
  return atual;
}

// Procedimento para consultar um valor da pilha
struct nodo * peek()
{
  //Cria um novo nodo com os dados do topo da pilha. Criar um novo evita
  //altera��es acidentais na pilha.     
  struct nodo *pnovo=NULL;
  if(topo != NULL){
     pnovo=(struct nodo*)malloc(sizeof(elemento));     
     pnovo->info = topo->info;
     pnovo->ant=NULL;
  }   
  return pnovo;
}


void menu(){
     printf("1 - Insere \n");
     printf("2 - Exclui \n");
     printf("3 - Consulta um valor\n");
     printf("0 - Fim\n");
}

int main(){
  struct nodo *elemento=NULL;
  char resp;
  int existe=0, valor=0;
  resp='n';
  topo=NULL;
  
  menu();
  fflush(stdin);
  printf("Digite sua opcao:\n");
  resp=getch();
  while((resp>='1') && (resp<='3')){
     switch(resp){
        case '1': printf("Digite o valor:\n");
                  scanf("%d",&valor);
                  fflush(stdin);
                  push(valor);
                  break;
                  
        case '2': elemento = pop();
                  if(elemento!=NULL)
                    printf("Retirado o valor %d\n",elemento->info);
                  break;
                  

        case '3': elemento = peek();
                  if(elemento!=NULL)
                    printf("Consultado o valor %d\n",elemento->info);
                  else
                    printf("Consultado o valor %d\n",elemento->info);
                  break;       
     }              
                   
     menu();
     fflush(stdin);             
	 printf("Digite sua opcao:\n");
	 resp=getch();
  }
}
