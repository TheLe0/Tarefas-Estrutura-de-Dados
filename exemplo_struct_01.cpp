#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<string.h>

#define TAM 50
#define NOME 20

struct funcionario{
   char nome[NOME];
   int idade;
};
struct funcionario lfuncionarios[TAM];
int qtde=0;

void insere(struct funcionario _func){
// Insere na lista os dados do funcionário.
     if(qtde < TAM){
        lfuncionarios[qtde] = _func;      
        qtde++;
     }
}

void listar(){
//Lista os funcionários
    printf("\n---------------------------------------------------------\n");
    for (int i=0; i<qtde; i++){
      printf("Nome do %uo Funcionario: %s\n", i+1, &lfuncionarios[i].nome);
      printf("Idade do %uo Funcionario: %u\n", i+1, lfuncionarios[i].idade);
      printf("\n");
   } 
   printf("---------------------------------------------------------\n");
}

void exclui(struct funcionario _func){
// Exclui um funcionario
   int achou = 0;
   int i = 0;
  
   while((achou==0) && (i<qtde)){ //procura o funcionário através do nome
      if(strcmp(_func.nome, lfuncionarios[i].nome) == 0){
         achou = 1;
      }
      else{
         i++;
      }
   }

   if(achou==1){ //achou o funcionario na lista
      for(int j=i;j<qtde-1;j++){ //exclui copiando o próximo para o lugar da exclusão
         lfuncionarios[j] = lfuncionarios[j+1];
      }
      qtde--; //diminui a quantidade de funcionários na lista
   }  
}

void opcoes(){
     printf("\n");
     printf("1 - Inserir Funcionario\n");
     printf("2 - Listar Funcionarios\n");
     printf("3 - Excluir Funcionario\n");
     printf("0 - Sair\n");
}
struct funcionario leDados(char situacao){
       struct funcionario func;
       func.nome[0]='\0'; //"Limpa" a string
       func.idade=0; //"Limpa" a idade
       
       printf("\nNome do Funcionario: ");
       fflush(stdin);
       scanf("%[^\n]s",&func.nome);
       //fgets(func.nome,NOME,stdin);
       
       if(situacao == 'I'){
              printf("Idade do Funcionario: ");
              fflush(stdin);
              scanf("%d", &func.idade);       
       }
       return func;
}
void menu(){
     char opc;
     char sit='I';
     struct funcionario func;
     system("cls");
     opcoes();
     opc = getch();
     while(opc != '0'){
          switch(opc){
             case '1': sit='I';
                       func = leDados(sit);
                       insere(func);
                       break;
             case '2': listar();
                       break;
             case '3': sit = 'E';
                       func = leDados(sit);
                       exclui(func);
                     break;
          }
          opcoes();
          opc = getch();     
     }
}

int main(){
   menu();
   system("pause");
   return 0;
}
