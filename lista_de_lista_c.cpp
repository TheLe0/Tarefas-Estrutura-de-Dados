#include <stdio.h>
#include<stdlib.h>
#include<string.h>
struct Hobby{
      char hobby[20];                  
      struct Hobby* prox;                   
};
struct Nodo{
      char nome[20];  
      struct Hobby* listaHobby;                
      struct Nodo* prox;
};

struct Nodo* inicio=NULL;

   bool isEmpty(){ 
     return (inicio==NULL); 
   }
//-------------------------------------------------------------
   void insereInicio(char nome[]){
      struct Nodo* novo = (struct Nodo*) malloc(sizeof(struct Nodo));
      strcpy(novo->nome,nome);
      novo->listaHobby = NULL;
      novo->prox = NULL;
      
      if(isEmpty()){
         inicio = novo;
      }
      else{
         novo->prox = inicio;
         inicio = novo;
      }
   }   
//-------------------------------------------------------------
   void insereHobby(char nome[], char hobby[]){
      struct Nodo* aux;
      aux = inicio;
      struct Hobby* auxLista;
      while(strcmp(aux->nome,nome)!=0){
         aux = aux->prox;
      }
      auxLista = aux->listaHobby;
      if(auxLista == NULL){
         struct Hobby* novoHobby;
         novoHobby = (struct Hobby*) malloc(sizeof(struct Hobby));           
         strcpy(novoHobby->hobby,hobby);
         novoHobby->prox=NULL;
         aux->listaHobby = novoHobby;
      }
      else{
           struct Hobby* novoHobby;
         novoHobby = (struct Hobby*) malloc(sizeof(struct Hobby));           
         strcpy(novoHobby->hobby,hobby);
         novoHobby->prox=aux->listaHobby;
         aux->listaHobby = novoHobby;  
      }
   } 
   
   void mostraLista(){
        struct Nodo* aux = inicio;
        while(aux != NULL){
           printf("%s\n",aux->nome);
           aux=aux->prox;
        }
   }

   void mostraListaCompleta(){
        struct Nodo* aux = inicio;
        struct Hobby* auxHobby;
        while(aux != NULL){
           printf("%s\n",aux->nome);
           auxHobby = aux->listaHobby;
           while(auxHobby != NULL){
              printf("   %s\n",auxHobby->hobby);
              auxHobby=auxHobby->prox;
           }
           aux=aux->prox;
        }
   }

 int main(){                            
      insereInicio("ana");    //insert 2 items
      insereInicio("andre");
      insereInicio("henrique");
 
      insereHobby("ana","leitura");
      insereHobby("ana","dormir");
      
      insereHobby("andre","correr");
      insereHobby("andre","viajar");
    
      insereHobby("henrique","nadar");
      insereHobby("henrique","brincar");
      insereHobby("henrique","fazer bagunca");
      
      mostraLista();
      mostraListaCompleta(); 
      system("pause");
      return 0;
 }  //end main()
